db.fruits.aggregate ([
	{ $match : { "supplier" : "Red Farms Inc."}},
	{ $count : "itemsInRedFarms"}
])



db.fruits.aggregate ([
	{ $match : { "price" : { $gt : 50 }}},
	{ $count : "NoOfItemsGreaterThanFifty"}
])



db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}}
])



db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}}
])



db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}}
])